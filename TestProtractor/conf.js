﻿// An example configuration file. 
exports.config = {
    framwork: 'jasmine',
    // The address of a running selenium server. 
    seleniumAddress: 'http://localhost:4444/wd/hub',
    specs: ['signup.js'], 
    // Capabilities to be passed to the webdriver instance. 
    capabilities: {
        'browserName': 'chrome'
    }    
};